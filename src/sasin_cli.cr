require "./sasin"

puts "sasin v. #{Sasin::VERSION} (c) 2020 Phitherek_"
input = if ARGV.any?
          ARGV[0]
        else
          print "Input: "
          gets
        end
if input.nil?
  puts "Input cannot be nil!"
else
  result = Sasin::Converter.convert(input.chomp)
  puts sprintf("Result: %.25f sasin", result)
end
