require "big/big_decimal"
module Sasin
  class Converter
    RATE = 70_000_000
    def self.convert(input : String|Int|Float|BigDecimal)
      (BigDecimal.new(input) / RATE).to_f64.round(25)
    end
  end
end
