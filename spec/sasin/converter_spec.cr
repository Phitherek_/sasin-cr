require "../spec_helper"

describe Sasin::Converter do
  describe "#convert" do
    it "should properly convert to sasin" do
      Sasin::Converter.convert(1).should eq(0.000000014285714285714286)
      Sasin::Converter.convert("1.5").should eq(0.000000021428571428571429)
      Sasin::Converter.convert(1.5).should eq(0.000000021428571428571429)
      Sasin::Converter.convert(BigDecimal.new("1.5")).should eq(0.000000021428571428571429)
      Sasin::Converter.convert(-1).should eq(-0.000000014285714285714286)
      Sasin::Converter.convert("-1.5").should eq(-0.000000021428571428571429)
      Sasin::Converter.convert(-1.5).should eq(-0.000000021428571428571429)
      Sasin::Converter.convert(BigDecimal.new("-1.5")).should eq(-0.000000021428571428571429)
    end
  end
end
