# Sasin (Crystal version)

This is a Crystal shard that implements a converter to an universal sasin unit.

## Installation

### Library

Add to `shard.yml` dependencies:

```yaml
dependencies:
  sasin:
    gitlab: Phitherek_/sasin-cr
    version: ~> 0.1.0
```

### Command line converter

* `shards build --production`


## Usage

### Converter class

```crystal
require "sasin"

Sasin::Converter.convert(input) # Returns value in sasin as Float64, input can be String, Int, Float or BigDecimal
```

### Converter app

* `bin/sasin` - the app will ask for input and output value in sasin
* `bin/sasin INPUT` - the app will output value in sasin

## Development

* Run `shards install`
* Add changes
* Run `crystal spec`
* Run `bin/ameba`

## Contributing

1. Fork it (<https://gitlab.com/Phitherek_/sasin-cr/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request

## Contributors

- [Phitherek_](https://gitlab.com/Phitherek_) - creator and maintainer

## Disclaimer

Yes, this shard is kind of a joke. It refers to a Polish minister of assets Jacek Sasin that spent a 70 million of Polish zloty on an election that didn't finally happen. It was also kind of inspired by a Python package of the same name.
